package Movement;
import java.io.IOException;
import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3GyroSensor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;
import lejos.utility.TextMenu;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;
import lejos.hardware.motor.Motor;

public class MoveTillSight{ 
	public static void main(String [] robot){
		Port portU = LocalEV3.get().getPort(portMenuUltra());
		EV3UltrasonicSensor sonic = new EV3UltrasonicSensor(portU);
		
		SampleProvider distance= sonic.getMode("Distance");
		float[] sample = new float[distance.sampleSize()];
		distance.fetchSample(sample, 0);
		while( (sample[0] > .5) && Button.ESCAPE.isUp() ){
			Motor.C.forward();
			Motor.B.forward();
			distance.fetchSample(sample, 0);
			LCD.drawString("" + sample[0],0,4);
		}
		
		Motor.C.flt();
		Motor.B.flt();
		sonic.close();
	
	}
	
	private static String portMenuUltra(){
		String ports[] = {"Port 1", "Port 2", "Port 3", "Port 4"};
        	TextMenu portMenu = new TextMenu(ports, 1, "Ultra port");
        	int number = portMenu.select() + 1;
        	LCD.clear();
        	return "S" + number;
    }
	
	private static String portMenuTouch(){
		String ports[] = {"Port 1", "Port 2", "Port 3", "Port 4"};
        	TextMenu portMenu = new TextMenu(ports, 1, "Touch port");
        	int number = portMenu.select() + 1;
        	LCD.clear();
        	return "S" + number;
    }
}